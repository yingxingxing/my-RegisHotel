const { defineConfig } = require('@vue/cli-service')
const url='http://localhost:8083/'
// let url="http://192.168.2.166/"
module.exports = defineConfig({
    transpileDependencies: true,
    lintOnSave: false,
    /**
     * open:自动打开
     * 目标代理:
     * proxy :代理控制
     * changeOrigin :允许跨域
     * target:跨域地址
     * pathRewrite 如果没访问"/api",重定向""
     * */
    devServer: {
        open: true,
        host: "localhost",
        port:"8081",
        proxy: {
            "/api": {
                target: url,
                ws: false,
                changeOrigin: true,
                pathRewrite: {
                    "^/api": " ",
                },
            },
        },
    },
})
