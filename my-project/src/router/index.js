import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/login',
        hidden:false,
        component: () => import('@/views/Login')
    },
    {
        path: '/login',
        name: 'Login',
        hidden:false,
        component: () => import('@/views/Login')
    },
    {
        path: '*',
        name:'NotFound',
        hidden:false,
        component: () => import('@/views/NotFound')
    },
    {
        path: '/homeView',
        name:"首页",
        hidden:true,
        iconClass:"el-icon-location",
        redirect: '/homeView/commission',
        component: () => import('@/views/HomeView'),
        children:[
            {
                path:"/homeView/commission",
                name:"当天代办业务",
                iconClass:"el-icon-location",
                component: () => import('@/views/homePage/Commission')
            },
            {
                path:"/homeView/dataAnalysis",
                name:"数据分析",
                iconClass:"el-icon-location",
                component: () => import('@/views/homePage/DataAnalysis')
            }

        ]
    },
    {
        path: '/homeView',
        name:"员工管理",
        hidden:true,
        iconClass:"el-icon-location",
        redirect: '/homeView/employee',
        component: () => import('@/views/HomeView'),
        children:[
            {
                path:"/homeView/employee",
                name:"员工列表",
                iconClass:"el-icon-location",
                component: () => import('@/views/staffManagement/Employee')
            },

        ]
    },
    {
        path: '/homeView',
        name:"业务管理",
        hidden:true,
        iconClass:"el-icon-location",
        redirect: '/homeView/sortManagement',
        component: () => import('@/views/HomeView'),
        children:[
            {
                path:"/homeView/sortManagement",
                name:"分类管理",
                iconClass:"el-icon-location",
                component: () => import('@/views/operationControl/SortManagement')
            },
            {
                path:"/homeView/cuisineManagement",
                name:"菜品管理",
                iconClass:"el-icon-location",
                component: () => import('@/views/operationControl/CuisineManagement')
            },
            {
                path:"/homeView/packageManagement",
                name:"套餐管理",
                iconClass:"el-icon-location",
                component: () => import('@/views/operationControl/PackageManagement')
            },
            {
                path:"/homeView/orderManagement",
                name:"订单管理",
                iconClass:"el-icon-location",
                component: () => import('@/views/operationControl/OrderManagement')
            },
            {
                path:"/homeView/globalState",
                name:"订单管理",
                iconClass:"el-icon-location",
                component: () => import('@/views/operationControl/globalState')
            },

        ]
    },


]
// 防止连续点击多次路由报错
let routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return routerPush.call(this, location).catch(err => err)
}

const router = new VueRouter({
    routes,
    mode:'history', //hash模式具备#号,
    scrollBehavior: () => ({ y: 0 }),
})
// 注册一个全局前置守卫
// router.beforeEach(async (to, from, next) => {
// /**
//  * to:即将要进入的目标用一种标准化的方式
//  * from: 当前导航正要离开的路由用一种标准化的方式
//  * */
//   // 如果用户访问的登录页,直接放行
//   if (to.name === 'Login') return next()
//   // 从sessionStorage中获取到保存的token值
// })
export default router
