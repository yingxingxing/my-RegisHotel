const TokenKey = 'Admin-Token'
export function getToken () {
    return sessionStorage.getItem(TokenKey)
}

export function setToken (token) {
    return sessionStorage.setItem(TokenKey, token)
}
export function getUserInfo (userInfo) {
    return sessionStorage.getItem(userInfo)
}

export function setUserInfo (userInfo,data) {
    return sessionStorage.setItem(userInfo, data)
}
