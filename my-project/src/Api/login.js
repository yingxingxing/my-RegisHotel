import request from '@/utils/request'
/**
 * 员工登录获取token值
 * */
export function login (data) {
    return request({
        url: '/employee/login',
        method: 'post',
        data
    })
}

/***
 * 员工退出登录
 */
export function logout () {
    return request({
        url: '/employee/logout',
        method: 'post',
    })
}
