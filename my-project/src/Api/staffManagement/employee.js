import request from '@/utils/request'
/**
 * 添加员工列表
 * */
export function AddStaff (data) {
    return request({
        url: '/employee',
        method: 'post',
        data
    })
}
/**
 * 员工列表分页查询
 * */
export function page (params) {
    return request({
        url: '/employee/page',
        method: 'get',
        params
    })
}
/**
 *根据id 的值修改员工状态
 * */
export function update (params) {
    return request({
        url: '/employee',
        method: 'put',
        data:params
    })
}
/**
 * 根据id查询员工信息
 * */
export function getById (id) {
    return request({
        url: `/employee/${id}`,
        method: 'get',
    })
}
