import request from '@/utils/request'

/**
 * 分类管理分页查询
 * */
export function page(params) {
    return request({
        url: '/category/page',
        method: 'get',
        params
    })
}

/**
 * 增加分类
 * */
export function add(data) {
    return request({
        url: '/category',
        method: 'post',
        data
    })
}

//
/**
 * 根据id删除分类
 * */
export function deletelist(params) {
    return request({
        url: `/category`,
        method: 'delete',
        params

    })
}

/**
 * 根据id修改分类
 * */
export function update(data) {
    return request({
        url: `/category`,
        method: 'put',
        data

    })
}
